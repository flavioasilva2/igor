package com.example.fafa.igor

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView


class EncountersAdapter (val encounterList : ArrayList<String>) : RecyclerView.Adapter<EncountersAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.encounter_card, parent, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return encounterList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    }

    class ViewHolder(itemView: View) :RecyclerView.ViewHolder(itemView){
        val textViewName = itemView.findViewById(R.id.encounter_card_name) as TextView
    }
}