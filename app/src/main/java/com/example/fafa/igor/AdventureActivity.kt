package com.example.fafa.igor

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.WindowManager
import android.widget.Toast
import com.google.firebase.database.*
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_adventure.*

class AdventureActivity : AppCompatActivity(), OnGoingAdventure.OnGoingAdventureListener, Players.PlayersListener {

    private var adventureId: String? = null
    private var mCurrentAdventure: Adventure? = null
    private var mAdventureRef: DatabaseReference? = null
    private var mAdventureOnGoing: Fragment? = null
    private var mAdventurePlayers: Fragment? = null
    private var mPicasso: Picasso? = null

    private val mOnGoingFragmentTAG = "ADVENTURE_ONGOING_FRAGMENT"
    private val mPlayersFragmentTAG = "ADVENTURE_PLAYERS_FRAGMENT"
    private val ADVENTURE_ID = "ADVENTURE_ID"
    private val TAG = "ADVENTURE_ACTIVITY_TAG"

    private val mAdventureImageResourceIDs = arrayOf(
            R.drawable.home_miniatura_coast,
            R.drawable.home_miniatura_corvali,
            R.drawable.home_miniatura_heartlands,
            R.drawable.home_miniatura_krevast,
            R.drawable.home_miniatura_imagem_automatica
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        intent?.extras?.apply {
            adventureId = getString(ADVENTURE_ID)
        }
        setContentView(R.layout.activity_adventure)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)
        mPicasso = Picasso.get()
        mAdventureRef = FirebaseDatabase.getInstance().getReference("/adventures/${adventureId}")
        mAdventureRef?.addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "${p0.toException()}")
            }

            override fun onDataChange(p0: DataSnapshot) {
                mCurrentAdventure = p0.getValue<Adventure>(Adventure::class.java)
                if(mCurrentAdventure == null) {
                    this@AdventureActivity.finish()
                    return
                }
                mCurrentAdventure?.title?.let {
                    textAdventureTitle?.text = it
                }
//                mPicasso?.load(mAdventureImageResourceIDs?.get(mCurrentAdventure?.backgroundImage!!))?.fit()?.into(imageAdventure)
//                mAdventureImageResourceIDs.get(mCurrentAdventure?.backgroundImage)
                mCurrentAdventure?.backgroundImage?.let {
                    mAdventureImageResourceIDs.get(it).let {
                        mPicasso?.load(it)?.fit()?.into(imageAdventure)
                    }
                }
            }
        })

        callOnGoingFragment()
    }



    override fun callOnGoingFragment() {
        mAdventureOnGoing = supportFragmentManager?.findFragmentByTag(mOnGoingFragmentTAG)
        if(mAdventureOnGoing == null)
            mAdventureOnGoing = OnGoingAdventure()
        var ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        ft?.replace(R.id.activity_adventure_fragment_container, mAdventureOnGoing, mOnGoingFragmentTAG)
        ft?.commit()
    }

    override fun callPlayersFragment() {
        mAdventurePlayers = supportFragmentManager?.findFragmentByTag(mPlayersFragmentTAG)
        if(mAdventurePlayers == null)
            mAdventurePlayers = Players()
        var ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        ft?.replace(R.id.activity_adventure_fragment_container, mAdventurePlayers, mPlayersFragmentTAG)
        ft?.commit()
    }
}