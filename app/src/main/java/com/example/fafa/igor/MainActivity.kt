package com.example.fafa.igor

import android.content.Intent
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.view.Gravity
import android.view.View
import android.view.MenuItem
import com.google.firebase.auth.FirebaseAuth
import android.widget.PopupMenu
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), Home.HomeFragListener, NewAdventure.NewAdventureFragListener, PopupMenu.OnMenuItemClickListener {

    private val mHomeFragmentTAG = "HOME_FRAGMENT"
    private val mNewAdventureFragmentTAG = "NEW_ADVENTURE_FRAGMENT"
    private val TAG = "MAIN_ACTIVITY_TAG"

    private var mAuth: FirebaseAuth? = null
    private var mHomeFragment: Home? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        mAuth = FirebaseAuth.getInstance()
        var currentUser = mAuth?.currentUser
        if(currentUser != null)
            callHomeFrag()
        else
            callAuthActivity()
    }

    override fun onStart() {
        super.onStart()
        //linhas para devolver a cor para os icones da gaveta
        navViewMain.itemIconTintList = null
        navViewMain.setNavigationItemSelectedListener {
            when(it.itemId) {
                R.id.gaveta_drawer -> {
                    activity_main.closeDrawer(Gravity.START)
                    true
                }
                R.id.gaveta_logout -> {
                    mAuth?.signOut()
                    callAuthActivity()
                    true
                }
                R.id.gaveta_combate -> {
                    callEncounterActivity()
                    true
                }
                else -> false
            }
        }

        //codigo para a gaveta
        setSupportActionBar(mainToolbar)
        toolbar_drawer.setOnClickListener {
            activity_main.openDrawer(Gravity.START)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mAuth = null
    }

    override fun callHomeFrag() {
        supportFragmentManager?.findFragmentByTag(mHomeFragmentTAG)?.let {
            mHomeFragment = it as Home
        }
        if(mHomeFragment == null)
            mHomeFragment = Home()
        var ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        ft?.replace(R.id.activity_main_fragment_container, mHomeFragment, mHomeFragmentTAG)
        ft?.commit()
    }

    override fun callAuthActivity() {
        var activityAuthIntent: Intent = Intent(this, AuthActivity::class.java)
        startActivity(activityAuthIntent)
        finish()
    }

    fun callEncounterActivity() {
        var encounterActivityIntent: Intent = Intent(this, Encounter::class.java)
        startActivity(encounterActivityIntent)
    }

    override fun callAdventureActivity(adventureId: String?) {
        var adventureActivityIntent: Intent = Intent(this, AdventureActivity::class.java)
        val ADVENTURE_ID = "ADVENTURE_ID"
        adventureActivityIntent.putExtra(ADVENTURE_ID, adventureId)
        startActivity(adventureActivityIntent)
    }

    fun showMenu(v: View) {
        PopupMenu(this, v).apply {
            setOnMenuItemClickListener(this@MainActivity)
            inflate(R.menu.dropdown_menu)
            show()
        }
    }

    //listener do dropdown menu
    override fun onMenuItemClick(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.dropdown_editar -> {
//                entra no modo de edição das aventuras
                mHomeFragment?.let {
                    it.togleEditMode()
                }
                true
            }
            R.id.dropdown_ordenar -> {
//                reordena a lista de aventuras
                mHomeFragment?.let {
                    it.toggleOrder()
                }
                true
            }
            else -> false

        }
    }

    override fun callNewAdventureFragment() {
        var newAdventureFragment = supportFragmentManager?.findFragmentByTag(mNewAdventureFragmentTAG)
        if(newAdventureFragment == null)
            newAdventureFragment = NewAdventure()
        var ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        ft?.replace(R.id.activity_main_fragment_container, newAdventureFragment, mNewAdventureFragmentTAG)
        ft?.addToBackStack(null)
        ft?.commit()
    }

    private fun toast(msg: String?){
        Toast.makeText(this@MainActivity, msg, Toast.LENGTH_SHORT).show()
    }
}
