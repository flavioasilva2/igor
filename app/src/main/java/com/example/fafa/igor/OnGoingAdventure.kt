package com.example.fafa.igor

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_adventure_on_going.*

class OnGoingAdventure : Fragment() {

    private var onGoingAdventureListener: OnGoingAdventureListener? = null
    private var mContext: Context? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is OnGoingAdventure.OnGoingAdventureListener){
            onGoingAdventureListener = context
        }else{
            throw ClassCastException(context.toString() + " must implement OnGoingAdventureListener.")
        }
        mContext = context
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_adventure_on_going, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickableButtonTabPlayers.setOnClickListener {
            onGoingAdventureListener?.callPlayersFragment()
        }
    }

    interface OnGoingAdventureListener{
        fun callOnGoingFragment()
        fun callPlayersFragment()
    }
}
