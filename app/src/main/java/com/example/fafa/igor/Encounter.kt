package com.example.fafa.igor

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.PopupMenu
import android.widget.Toast
import java.util.*

class Encounter : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_encounter)

        val recycler = findViewById<RecyclerView>(R.id.encounter_recyclerview)
        recycler.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL, false)
        val elementos = ArrayList<String>()
        elementos.add("huehue")
        elementos.add("huehue")
        elementos.add("huehue")
        elementos.add("huehue")
        elementos.add("huehue")
        elementos.add("huehue")
        elementos.add("huehue")
        elementos.add("huehue")


        val adapter = EncountersAdapter(elementos)

        recycler.adapter = adapter
    }

    fun goToMainActivity (v: View) {
        var mainActivityIntent: Intent = Intent(this, MainActivity::class.java)
        startActivity(mainActivityIntent)
    }

    fun showDiceMenu(v: View) {
        val popupMenu = PopupMenu(this, v)
        val inflater: MenuInflater = popupMenu.menuInflater
        inflater.inflate(R.menu.dice_menu, popupMenu.menu)

        Log.d("dice menu","inflating")

        popupMenu.setOnMenuItemClickListener(PopupMenu.OnMenuItemClickListener { item: MenuItem? ->

            Log.d("dice menu", "WAS I HERE?")
            when (item?.itemId) {
                R.id.dice_d4 -> {
                    Log.d("dice menu", "d4")
                    val intAleatorio = (1..4).shuffled().last()
                    Toast.makeText(this@Encounter, intAleatorio.toString(), Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.dice_d6 -> {
                    Log.d("dice menu", "d6")
                    val intAleatorio = (1..6).shuffled().last()
                    Toast.makeText(this@Encounter, intAleatorio.toString(), Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.dice_d8 -> {
                    Log.d("dice menu", "d8")
                    val intAleatorio = (1..8).shuffled().last()
                    Toast.makeText(this@Encounter, intAleatorio.toString(), Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.dice_d10 -> {
                    Log.d("dice menu", "d10")
                    val intAleatorio = (1..10).shuffled().last()
                    Toast.makeText(this@Encounter, intAleatorio.toString(), Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.dice_d12 -> {
                    Log.d("dice menu", "d12")
                    val intAleatorio = (1..12).shuffled().last()
                    Toast.makeText(this@Encounter, intAleatorio.toString(), Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.dice_d20 -> {
                    Log.d("dice menu", "d20")
                    val intAleatorio = (1..20).shuffled().last()
                    Toast.makeText(this@Encounter, intAleatorio.toString(), Toast.LENGTH_SHORT).show()
                    true
                }
            }
            Log.d("dice menu", "menu id = " + item?.itemId)
            true
        })

        popupMenu.show()
    }
}
