package com.example.fafa.igor

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.fragment_new_adventure.*

class NewAdventure : Fragment() {

    private var mAdventuresRef: DatabaseReference? = null
    private var mAuth: FirebaseAuth? = null
    private var mNewAdventureFragListener: NewAdventureFragListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is NewAdventure.NewAdventureFragListener){
            mNewAdventureFragListener = context
        }else{
            throw ClassCastException(context.toString() + " must implement NewAdventureFragListener.")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        mAdventuresRef = FirebaseDatabase.getInstance().getReference("/adventures")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_new_adventure, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragment_new_adventure_ready?.setOnClickListener {
            newAdventure(inputNomeAventura?.text?.toString())
            mNewAdventureFragListener?.callHomeFrag()
        }
    }

    private fun newAdventure(title: String?) {
        var adventure = Adventure()
        adventure.creator = mAuth?.currentUser?.uid
        adventure.title = title!!
        adventure.backgroundImage = randomIndex(5)
        adventure.progress = 0
        var key = mAdventuresRef?.push()?.key
        mAdventuresRef?.child(key!!)?.setValue(adventure)
    }

    private fun randomIndex(arraySize: Int): Int = (0..(arraySize - 1)).shuffled().last()

    interface NewAdventureFragListener {
        fun callHomeFrag()
    }
}