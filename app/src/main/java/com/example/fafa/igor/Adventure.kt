package com.example.fafa.igor

import com.google.firebase.database.IgnoreExtraProperties

@IgnoreExtraProperties
class Adventure (){
    var title: String? = null
    var nextSession: String? = null
    var progress: Int? = null
    var backgroundImage: Int = -1
    var creator: String? = null
//    var sessions: ArrayList<>? = null
//    var description: String? = null
}