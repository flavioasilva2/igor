package com.example.fafa.igor

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v4.app.FragmentTransaction
import android.view.View
import android.view.WindowManager

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser

class AuthActivity : AppCompatActivity(), Login.LoginFragListener, Signup.SignupFragListener, PasswordRecovery.PasswordRecoveryFragListener {

    private val mLoginFragmentTAG = "LOGIN_FRAGMENT"
    private val mSignupFragmentTAG = "SIGNUP_FRAGMENT"
    private val mPasswordRecoveryFragmentTAG = "PASSWORD_RECOVERY_FRAGMENT"
    private val mParamKeepLogged = "KEEP_LOGGED"

    private var mKeepLogged: Boolean = false
    private var mAuth: FirebaseAuth? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let {
            mKeepLogged = it.getBoolean(mParamKeepLogged)
        }
        setContentView(R.layout.activity_auth)
        window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN)

        mAuth = FirebaseAuth.getInstance()
    }

    override fun onStart() {
        super.onStart()
        var currentUser: FirebaseUser? = mAuth?.currentUser

        if(currentUser == null)
            callLoginFrag()
        else
            callMainActivity()
    }

    override fun onSaveInstanceState(outState: Bundle?, outPersistentState: PersistableBundle?) {
        outState?.let {
            it.putBoolean(mParamKeepLogged, mKeepLogged)
        }
        outPersistentState?.let {
            it.putBoolean(mParamKeepLogged, mKeepLogged)
        }
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onStop() {
        super.onStop()
        if(!mKeepLogged){
            mAuth?.signOut()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mAuth = null
    }

    override fun callMainActivity() {
        var activityMainIntent: Intent = Intent(this, MainActivity::class.java)
        activityMainIntent.putExtra(mParamKeepLogged, mKeepLogged)
        startActivity(activityMainIntent)
        finish()
    }

    override fun callSignupFrag() {
        var signupFragment = supportFragmentManager?.findFragmentByTag(mSignupFragmentTAG)
        if(signupFragment == null)
            signupFragment = Signup()
        var ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        ft?.replace(R.id.activity_auth_fragment_container, signupFragment, mSignupFragmentTAG)
        ft?.addToBackStack(null)
        ft?.commit()
    }

    override fun setLoginPersistenceState(state: Boolean){
        mKeepLogged = state
    }

    override fun callLoginFrag() {
        var loginFragment = supportFragmentManager?.findFragmentByTag(mLoginFragmentTAG)
        if(loginFragment == null)
            loginFragment = Login()
        var ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        ft?.replace(R.id.activity_auth_fragment_container, loginFragment, mLoginFragmentTAG)
        ft?.commit()
    }

    override fun callPasswordRecoveryFrag() {
        var passwordRecoveryFragment = supportFragmentManager?.findFragmentByTag(mPasswordRecoveryFragmentTAG)
        if(passwordRecoveryFragment == null)
            passwordRecoveryFragment = PasswordRecovery()
        var ft: FragmentTransaction? = supportFragmentManager.beginTransaction()
        ft?.replace(R.id.activity_auth_fragment_container, passwordRecoveryFragment, mPasswordRecoveryFragmentTAG)
        ft?.addToBackStack(null)
        ft?.commit()
    }
}
