package com.example.fafa.igor

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_adventure_players.*

class Players : Fragment() {

    private var playersListener: Players.PlayersListener? = null
    private var mContext: Context? = null
    private var mPicasso: Picasso? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is Players.PlayersListener){
            playersListener = context
        }else{
//            throw ClassCastException(context.toString() + " must implement PlayersListener.")
        }
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPicasso = Picasso.get()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_adventure_players, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickableButtonTabOnGoing?.setOnClickListener {
            playersListener?.callOnGoingFragment()
        }
        mPicasso?.load(R.drawable.players_caixa_de_conteudo)?.fit()?.into(imageContentTab)
        mPicasso?.load(R.drawable.players_capitao2)?.fit()?.into(profile_image1)
        mPicasso?.load(R.drawable.players_tupac)?.fit()?.into(profile_image2)
        mPicasso?.load(R.drawable.players_snopp_dog)?.fit()?.into(profile_image3)
        mPicasso?.load(R.drawable.players_eminem)?.fit()?.into(profile_image4)
    }

    override fun onDetach() {
        super.onDetach()
        playersListener = null
        mContext = null
        mPicasso = null
    }

    interface PlayersListener{
        fun callPlayersFragment()
        fun callOnGoingFragment()
    }
}
