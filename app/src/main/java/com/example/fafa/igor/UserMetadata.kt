package com.example.fafa.igor

import com.google.firebase.database.IgnoreExtraProperties
import java.util.Date

@IgnoreExtraProperties
class UserMetadata (userName: String?, birthDate: Date?, gender: String?){
    val userName: String? = userName
    val birthDate: Date? = birthDate
    val gender: String? = gender
}