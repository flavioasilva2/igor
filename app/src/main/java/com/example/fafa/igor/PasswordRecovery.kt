package com.example.fafa.igor

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_password_recovery.*

class PasswordRecovery : Fragment() {

    private var mPicasso: Picasso? = null
    private var mAuth: FirebaseAuth? = null
    private var mContext: Context? = null
    private var mPasswordRecoveryFragListener: PasswordRecoveryFragListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is PasswordRecoveryFragListener) {
            mPasswordRecoveryFragListener = context
        }else{
            throw ClassCastException(context.toString() + " must implement PasswordRecoveryFragListener.")
        }
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPicasso = Picasso.get()
        mAuth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_password_recovery, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPicasso?.load(R.drawable.igor_login_backgrownd)?.fit()?.into(fragment_password_recovery_background)
        mPicasso?.load(R.drawable.igor_login_igor_icon)?.fit()?.into(fragment_password_recovery_logo)
    }

    override fun onStart() {
        super.onStart()
        fragment_password_recovery_button.setOnClickListener {
            recoveryPass()
        }
    }

    override fun onResume() {
        super.onResume()
        if(!isFieldEmpty(fragment_password_recovery_user_edit_text))
            fragment_password_recovery_user_edit_text?.text =  null
    }

    private fun recoveryPass(){
        clearEditTextError(fragment_password_recovery_user_edit_text)
        if (isFieldEmpty(fragment_password_recovery_user_edit_text)){
            fragment_password_recovery_user_edit_text.setError("Campo obrigatório")
            return
        }

        val email = fragment_password_recovery_user_edit_text.text.toString()

        mAuth?.sendPasswordResetEmail(email)
        toast("Email enviado")
        mPasswordRecoveryFragListener?.callLoginFrag()
    }

    private fun isFieldEmpty(view: EditText): Boolean{
        var text: String? = null
        view.let {
            text = it.text?.toString()
        }
        if(text?.length == 0) return true
        return false
    }

    private fun clearEditTextError(view: EditText){
        view.let {
            it.setError(null)
        }
    }

    private fun toast(msg: String?){
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show()
    }

    interface PasswordRecoveryFragListener{
        fun callLoginFrag()
    }
}
