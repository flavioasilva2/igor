package com.example.fafa.igor

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.fragment_home.*
import java.util.TreeMap
import kotlin.collections.ArrayList

class Home : Fragment(), AdventureAdapter.AdventureAdapterListener {

    private var mAuth: FirebaseAuth? = null
    private var mHomeFragListener: HomeFragListener? = null
    private var mAdventuresList: ArrayList<Adventure>? = null
    private var mSortedKeys: ArrayList<String>? = null
    private var mAdventuresHashMap: HashMap<String, Adventure>? = null
    private var mRecyclerView: RecyclerView? = null
    private var mRecyclerViewAdapter: AdventureAdapter? = null
    private var mContext: Context? = null
    private var mAdventuresRef: DatabaseReference? = null
    private var mNormalListOrder: Boolean = true
    private var mEditMode: Boolean = false

    private val TAG = "HOME_TAG"

    private val mAdventureImageResourceIDs = arrayOf(
            R.drawable.home_miniatura_coast,
            R.drawable.home_miniatura_corvali,
            R.drawable.home_miniatura_heartlands,
            R.drawable.home_miniatura_krevast,
            R.drawable.home_miniatura_imagem_automatica
    )


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is HomeFragListener){
            mHomeFragListener = context
        }else{
            throw ClassCastException(context.toString() + " must implement HomeFragListener.")
        }
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        mAdventuresRef = FirebaseDatabase.getInstance().getReference("/adventures")
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view = inflater.inflate(R.layout.fragment_home, container, false)
        mRecyclerView = view.findViewById(R.id.fragment_home_adventures_recyclerview)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fragment_home_new_adventure?.setOnClickListener {
            mHomeFragListener?.callNewAdventureFragment()
        }
    }

    override fun onStart() {
        super.onStart()
        mAdventuresList = ArrayList()
        mAdventuresHashMap = HashMap()
        mRecyclerViewAdapter = AdventureAdapter(mAdventuresList, mContext, mAdventureImageResourceIDs, this, mAuth?.currentUser?.uid!!)
        mRecyclerView?.adapter = mRecyclerViewAdapter
        mRecyclerView?.setHasFixedSize(true)
        mRecyclerView?.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)

        mAdventuresRef?.addChildEventListener(object : ChildEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Log.w(TAG, "onCancelled: ${p0.toException()}")
            }

            override fun onChildAdded(p0: DataSnapshot, p1: String?) {
                var adventure: Adventure = p0.getValue<Adventure>(Adventure::class.java)!!
                mAdventuresHashMap?.put(p0.key!!, adventure)
                var sortedMap: TreeMap<String, Adventure> = TreeMap(mAdventuresHashMap)
                if (mNormalListOrder)
                    mSortedKeys = ArrayList(sortedMap.keys)
                else
                    mSortedKeys = ArrayList(sortedMap.keys.reversed())
                mAdventuresList?.clear()
                mSortedKeys?.forEach {
                    mAdventuresList?.add(((mAdventuresHashMap!!)[it])!!)
                    Unit
                }
                mRecyclerViewAdapter?.notifyDataSetChanged()
            }

            override fun onChildChanged(p0: DataSnapshot, p1: String?) {
                var adventure: Adventure = p0.getValue<Adventure>(Adventure::class.java)!!
                mAdventuresHashMap?.remove(p0.key!!)
                mAdventuresHashMap?.put(p0.key!!, adventure)
                var sortedMap: TreeMap<String, Adventure> = TreeMap(mAdventuresHashMap)
                if (mNormalListOrder)
                    mSortedKeys = ArrayList(sortedMap.keys)
                else
                    mSortedKeys = ArrayList(sortedMap.keys.reversed())
                mAdventuresList?.clear()
                mSortedKeys?.forEach {
                    mAdventuresList?.add(((mAdventuresHashMap!!)[it])!!)
                    Unit
                }
                mRecyclerViewAdapter?.notifyDataSetChanged()
            }

            override fun onChildMoved(p0: DataSnapshot, p1: String?) {
                Log.i(TAG, "onChildMoved")
            }

            override fun onChildRemoved(p0: DataSnapshot) {
                var key = p0.key!!
                var sortedMap = TreeMap<String, Adventure>(mAdventuresHashMap)
                mSortedKeys = ArrayList(sortedMap.keys)
                var pos = mSortedKeys?.indexOf(key)!!
                mAdventuresHashMap?.remove(key)
                sortedMap = TreeMap<String, Adventure>(mAdventuresHashMap)
                mSortedKeys = ArrayList(sortedMap.keys)
                mAdventuresList?.clear()
                mSortedKeys?.forEach {
                    mAdventuresList?.add(((mAdventuresHashMap!!)[it])!!)
                    Unit
                }
                mRecyclerViewAdapter?.notifyItemRemoved(pos);
                mRecyclerViewAdapter?.notifyItemRangeChanged(pos, mAdventuresList?.size!!);
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        mAuth = null
        mHomeFragListener = null
        mAdventuresList = null
        mAdventuresHashMap = null
        mRecyclerView = null
        mContext = null
        mAdventuresRef = null
        mRecyclerViewAdapter = null
    }

    private fun randomIndex(arraySize: Int): Int = (0..(arraySize - 1)).shuffled().last()

    override fun onClickAdventureCard(itemView: AdventureAdapter.ViewHolder) {
        mHomeFragListener?.callAdventureActivity(mSortedKeys?.get(itemView.vhPos))
    }

    override fun deleteAdventure(index: Int) {
        var sortedMap: TreeMap<String, Adventure>? = null
        mAdventuresHashMap?.let {
            sortedMap = TreeMap(it)
        }
        if (mNormalListOrder)
            mSortedKeys = ArrayList(sortedMap?.keys)
        else
            mSortedKeys = ArrayList(sortedMap?.keys?.reversed())
        var deletedAdventureKey: String? = mSortedKeys?.get(index)
        mAdventuresRef?.child(deletedAdventureKey!!)?.removeValue()
    }

    fun toggleOrder(){
        mNormalListOrder = !mNormalListOrder

        var sortedMap: TreeMap<String, Adventure>? = null
        mAdventuresHashMap?.let {
                sortedMap = TreeMap(it)
        }

        if (mNormalListOrder)
            mSortedKeys = ArrayList(sortedMap?.keys)
        else
            mSortedKeys = ArrayList(sortedMap?.keys?.reversed())
        mAdventuresList?.clear()
        mSortedKeys?.forEach {
            mAdventuresList?.add(((mAdventuresHashMap!!)[it])!!)
            Unit
        }
        recyclerViewRedraw()
    }

    fun togleEditMode(){
        mEditMode = !mEditMode
        if(mEditMode){
            fragment_home_new_adventure?.visibility = View.GONE
        }
        else{
            fragment_home_new_adventure?.visibility = View.VISIBLE
        }
        mRecyclerViewAdapter?.setEditMode(mEditMode)
        recyclerViewRedraw()
    }

    private fun recyclerViewRedraw(){
        mRecyclerView?.adapter = null
        mRecyclerView?.layoutManager = null
        mRecyclerView?.adapter = mRecyclerViewAdapter
        mRecyclerView?.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        mRecyclerViewAdapter?.notifyDataSetChanged()
    }

    interface HomeFragListener{
        fun callHomeFrag()
        fun callAuthActivity()
        fun callAdventureActivity(adventureId: String?)
        fun callNewAdventureFragment()
    }
}
