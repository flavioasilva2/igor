package com.example.fafa.igor

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_login.*

class Login : Fragment() {

    private val mParamKeepLogged = "KEEP_LOGGED"

    private var mPicasso: Picasso? = null
    private var mKeepLogged: Boolean = false
    private var mAuth: FirebaseAuth? = null
    private var mContext: Context? = null
    private var mLoginFragListener: LoginFragListener? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is LoginFragListener) {
            mLoginFragListener = context
        }else{
            throw ClassCastException(context.toString() + " must implement LoginFragListener.")
        }
        mContext = context
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        savedInstanceState?.let{
            mKeepLogged = it.getBoolean(mParamKeepLogged)
        }
        mPicasso = Picasso.get()
        mAuth = FirebaseAuth.getInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_login, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPicasso?.load(R.drawable.igor_login_backgrownd)?.fit()?.into(fragment_login_background)
        mPicasso?.load(R.drawable.igor_login_igor_icon)?.fit()?.into(fragment_login_logo)
        if(mKeepLogged)
            mPicasso?.load(R.drawable.igor_login_box_checked)?.fit()?.into(fragment_login_keep_loged_check)
        else
            mPicasso?.load(R.drawable.igor_login_box_unchecked)?.fit()?.into(fragment_login_keep_loged_check)
    }

    override fun onStart() {
        super.onStart()
        fragment_login_keep_loged_check.setOnClickListener {
            toggleKeepLogedCheck()
        }
        fragment_login_keep_loged_check_text_view.setOnClickListener {
            toggleKeepLogedCheck()
        }
        fragment_login_login_button.setOnClickListener {
            doLogin()
        }
        fragment_login_create_account_textview.setOnClickListener {
            mLoginFragListener?.callSignupFrag()
        }
        fragment_login_forgot_pass_textview.setOnClickListener {
            mLoginFragListener?.callPasswordRecoveryFrag()
        }
        fragment_login_user_edit_text.requestFocus()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.let {
            it.putBoolean(mParamKeepLogged, mKeepLogged)
        }
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        mPicasso = null
        mAuth = null
        mContext = null
        mLoginFragListener = null
    }

    private fun toggleKeepLogedCheck(){
        mKeepLogged = !mKeepLogged
        if(mKeepLogged)
            mPicasso?.load(R.drawable.igor_login_box_checked)?.fit()?.into(fragment_login_keep_loged_check)
        else
            mPicasso?.load(R.drawable.igor_login_box_unchecked)?.fit()?.into(fragment_login_keep_loged_check)
    }

    private fun doLogin(){
        fragment_login_user?.error = null
        fragment_login_password?.error = null

        val mail: String = fragment_login_user_edit_text?.text.toString()
        val pass: String = fragment_login_password_edit_text?.text.toString()

        if(mail.length == 0){
            fragment_login_user?.error = "Campo obrigatório"
            return
        }
        if(pass.length == 0){
            fragment_login_password?.error = "Campo obrigatório"
            return
        }

        mLoginFragListener?.setLoginPersistenceState(mKeepLogged)

        mAuth?.signInWithEmailAndPassword(mail, pass)?.addOnCompleteListener {
            mAuth?.currentUser?.let {
                mLoginFragListener?.callMainActivity()
            }
        }?.addOnFailureListener {
            toast("Login failed: " + it.message)
        }
    }

    private fun toast(msg: String?){
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show()
    }

    interface LoginFragListener{
        fun callMainActivity()
        fun callSignupFrag()
        fun setLoginPersistenceState(state: Boolean)
        fun callPasswordRecoveryFrag()
    }
}
