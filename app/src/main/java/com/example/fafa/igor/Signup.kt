package com.example.fafa.igor

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_signup.*
import java.text.SimpleDateFormat
import java.util.Date

class Signup : Fragment() {

    private var mPicasso: Picasso? = null
    private var mAuth: FirebaseAuth? = null
    private var mSignupFragListener: SignupFragListener? = null
    private var mDatabase: FirebaseDatabase? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if(context is SignupFragListener) {
            mSignupFragListener = context
        }else{
            throw ClassCastException(context.toString() + " must implement SignupFragListener.")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mPicasso = Picasso.get()
        mAuth = FirebaseAuth.getInstance()
        mDatabase = FirebaseDatabase.getInstance()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_signup, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mPicasso?.load(R.drawable.igor_login_backgrownd)?.fit()?.into(fragment_signup_background)
        mPicasso?.load(R.drawable.igor_login_igor_icon)?.fit()?.into(fragment_signup_logo)
    }

    override fun onStart() {
        super.onStart()
        fragment_signup_create_button.setOnClickListener {
            createUser()
        }
        fragment_signup_email_edit_text.requestFocus()
    }

    override fun onDestroy() {
        super.onDestroy()
        mPicasso = null
        mAuth = null
    }

    private fun createUser(){
        var signupEditTextViews = arrayOf(
                fragment_signup_email_edit_text,
                fragment_signup_pass_edit_text,
                fragment_signup_user_name_edit_text,
                fragment_signup_birth_date_edit_text,
                fragment_signup_gender_edit_text
        )

        signupEditTextViews.forEach {
            clearEditTextError(it)
        }
        var error = false
        signupEditTextViews.forEach {
            if(isFieldEmpty(it)){
                it.setError("Campo obrigatório")
                error = true
            }
        }
        if (error) return

        val user = fragment_signup_email_edit_text.text?.toString()
        val pass = fragment_signup_pass_edit_text.text?.toString()
        val userName = fragment_signup_user_name_edit_text.text?.toString()
        val birthDateString = fragment_signup_birth_date_edit_text.text?.toString()
        val gender = fragment_signup_gender_edit_text.text?.toString()

        val databaseReference: DatabaseReference? = mDatabase?.getReference("users")

        mAuth?.createUserWithEmailAndPassword(user!!, pass!!)?.addOnCompleteListener {
            if (it.isSuccessful()){
                val userUid = mAuth?.currentUser?.uid
                val birthDate:Date? = getUserBirthDate(birthDateString)

                val userMetadata = UserMetadata(userName, birthDate, gender)

                databaseReference?.child(userUid!!)?.setValue(userMetadata)
                mSignupFragListener?.callMainActivity()
            }
        }
    }

    private fun isFieldEmpty(view: EditText): Boolean{
        var text: String? = null
        view.let {
            text = it.text?.toString()
        }
        if(text?.length == 0) return true
        return false
    }

    private fun clearEditTextError(view: EditText){
        view.let {
            it.setError(null)
        }
    }

    private fun getUserBirthDate(birthDate: String?): Date {
        val partten = "dd/MM/yyyy"
        val format = SimpleDateFormat(partten)

        return format.parse(birthDate)
    }

    interface SignupFragListener {
        fun callMainActivity()
    }
}
