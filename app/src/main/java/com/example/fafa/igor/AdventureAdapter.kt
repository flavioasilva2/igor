package com.example.fafa.igor;

import android.content.Context
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.squareup.picasso.Picasso

class AdventureAdapter(adventureList: ArrayList<Adventure>?, context: Context?, imageResourceIDs: Array<Int>?, fragment: Home?, userId: String) : RecyclerView.Adapter<AdventureAdapter.ViewHolder>() {

    private var mAdventureList: ArrayList<Adventure>? = null
    private var mContext: Context? = null
    private var mAdventureAdapterListener: AdventureAdapterListener? = null
    private var mImageResourceIDs: Array<Int>? = null
    private var mPicasso: Picasso? = null
    private var mEditMode: Boolean = false
    private var mCurrentUserId: String? = null

    init {
        mAdventureList = adventureList
        mContext = context
        mImageResourceIDs = imageResourceIDs
        mPicasso = Picasso.get()
        mCurrentUserId = userId
        if(fragment is AdventureAdapterListener) {
            mAdventureAdapterListener = fragment
        }else{
            throw ClassCastException(context.toString() + " must implement AdventureAdapterListener.")
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val mTitle = itemView.findViewById<TextView>(R.id.adventure_title)
        private val mNextSession = itemView.findViewById<TextView>(R.id.adventure_next_session)
        private val mBackground = itemView.findViewById<AppCompatImageView>(R.id.adventure_card_background)
        private val mProgress = itemView.findViewById<ProgressBar>(R.id.adventure_progress)
        private val mDeleteButton = itemView.findViewById<Button>(R.id.adventure_card_exclude_button)
        private val mOverlay = itemView.findViewById<ImageView>(R.id.adventure_card_overlay)
        var vhPos: Int = -1

        init {
            itemView.setOnClickListener {
                mAdventureAdapterListener?.onClickAdventureCard(this)
            }
        }

        fun setTitle(title: String?){
            mTitle?.text = title
        }

        fun setNextSession(nextSession: String?){
            var str: String? = mContext?.getString(R.string.fragment_home_adventure_next_session)
            mNextSession?.text = "$str $nextSession"
        }

        fun setBackground(imageResourceID: Int){
            mPicasso?.load(mImageResourceIDs?.get(imageResourceID)!!)?.fit()?.into(mBackground)
        }

        fun setProgress(progress: Int){
            mProgress?.progress = progress
        }

        fun setDeleteButtonVisibility(visible: Boolean){
            if(visible)
                mDeleteButton?.visibility = View.VISIBLE
            else
                mDeleteButton?.visibility = View.GONE
        }

        fun setOverlayVisibility(visible: Boolean){
            if(visible)
                mOverlay?.visibility = View.VISIBLE
            else
                mOverlay?.visibility = View.GONE
        }

        fun setDeleteButtonIndex(index: Int){
            mDeleteButton?.setOnClickListener {
                mAdventureAdapterListener?.deleteAdventure(index)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var inflater: LayoutInflater = LayoutInflater.from(mContext);
        var view: View = inflater.inflate(R.layout.adventure_card, parent, false);
        var vh = ViewHolder(view);
        return vh;
    }

    override fun onBindViewHolder(holder: AdventureAdapter.ViewHolder, position: Int) {
        val adventure: Adventure? = mAdventureList?.get(position)

        adventure?.title?.let {
            holder.setTitle(it)
        }
        adventure?.nextSession?.let {
            holder.setNextSession(it)
        }
        adventure?.progress?.let {
            holder.setProgress(it)
        }
        adventure?.backgroundImage?.let {
            holder.setBackground(it)
        }
        if(mEditMode){
            if(adventure?.creator != null) {
                if(adventure.creator == mCurrentUserId){
                    holder.setDeleteButtonVisibility(true)
                    holder.setOverlayVisibility(false)
                }else{
                    holder.setOverlayVisibility(true)
                    holder.setDeleteButtonVisibility(false)
                }
            }else{
                holder.setOverlayVisibility(true)
                holder.setDeleteButtonVisibility(false)
            }
        }else{
            holder.setDeleteButtonVisibility(false)
            holder.setOverlayVisibility(false)
        }
        holder.setDeleteButtonIndex(position)
        holder.vhPos = position
    }

    override fun getItemCount(): Int {
        mAdventureList?.let {
            return it.size
        }
        return 0;
    }

    fun setEditMode(mode: Boolean){
        mEditMode = mode
    }

    interface AdventureAdapterListener{
        fun onClickAdventureCard(itemView: ViewHolder)
        fun deleteAdventure(index: Int)
    }
}
